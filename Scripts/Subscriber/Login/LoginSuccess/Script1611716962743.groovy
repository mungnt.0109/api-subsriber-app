import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject


import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import groovy.json.JsonSlurper as JsonSlurper
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint

response = WS.sendRequestAndVerify(findTestObject("Subscriber/Login/Login",
				["phone": phone, "password": password]))
//after request, API will response the json
//using JsonSlurper support parse json to text
JsonSlurper jsonSlurper = new JsonSlurper()
Object jsonResponse = jsonSlurper.parseText(response.getResponseText())
println "jsonResponse: " + jsonResponse.getAt("message")
//get message element
//if you get the token, plz try token.token
WS.verifyElementPropertyValue(response, "message", "Success")
//set the token Globalvariable for using next step
GlobalVariable.token = jsonResponse.getAt("token").getAt("token").toString()
//will print out console, plz check console tab
println "GlobalVariable.token: " + GlobalVariable.token
//to use the token globalvariable, u should run into the test suite, cannot run single test case/object repository
//CustomKeywords.'help.apiMapping.login'(phone, password, deviceId, firebaseToken)

