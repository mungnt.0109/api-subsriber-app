import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper as JsonSlurper


JsonSlurper jsonSlurper = new JsonSlurper()

response1 = WS.sendRequestAndVerify(findTestObject("Subscriber/Move money/1.1 Move money bank-wallet RequestTransaction",
	["SENDERPHONE": SENDERPHONE, "AMOUNT": AMOUNT]))

WS.verifyElementPropertyValue(response1, "message", exp_message)

Object jsonResponse1 = jsonSlurper.parseText(response1.getResponseText())

println "jsonResponse: " + jsonResponse1.getAt("message")

GlobalVariable.TxnID = jsonResponse1.getAt("data").getAt("TRANSREFID").toString()

println "GlobalVariable.TxnID: " + GlobalVariable.TxnID

response2 = WS.sendRequestAndVerify(findTestObject("Subscriber/Move money/1.2 Move money bank-wallet ConfirmTransaction"))

WS.verifyElementPropertyValue(response2, "message", exp_message)

response3 = WS.sendRequestAndVerify(findTestObject("Subscriber/Move money/1.3 Move money bank-wallet VerifyTransaction",["OTP": OTP,"PIN":PIN]))

WS.verifyElementPropertyValue(response3, "message", exp_message)