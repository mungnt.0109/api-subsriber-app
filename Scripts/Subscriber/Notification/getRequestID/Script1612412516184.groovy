import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint

import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper as JsonSlurper

response = WS.sendRequestAndVerify(findTestObject("Object Repository/Subscriber/Notification/listNoti"))

WS.verifyElementPropertyValue(response, "message", "Success")

JsonSlurper jsonSlurper = new JsonSlurper()

Object jsonResponse = jsonSlurper.parseText(response.getResponseBodyContent())

title = jsonResponse.data[0].title

println "title: " + title

WS.verifyElementPropertyValue(response, "data[0].title", "Request pay merchant")

GlobalVariable.requestId = jsonResponse.data[0].cmd.requestId

println "GlobalVariable.requestId: " + GlobalVariable.requestId

//GlobalVariable.notiId = jsonResponse.data[0].id
//
//println "GlobalVariable.notiId: " + GlobalVariable.notiId
//
//response2 = WS.sendRequestAndVerify(findTestObject("Object Repository/Subscriber/Notification/detailNotification"))
//
//WS.verifyElementPropertyValue(response, "message", "Success")

