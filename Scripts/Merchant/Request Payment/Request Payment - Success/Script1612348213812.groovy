import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import groovy.json.JsonSlurper as JsonSlurper
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

response = WS.sendRequestAndVerify(findTestObject("Merchant/Request Pay/Request Payment",["phone": phone, "amount": amount]))

JsonSlurper jsonSlurper = new JsonSlurper()

Object jsonResponse = jsonSlurper.parseText(response.getResponseText())

println "jsonResponse: " + jsonResponse.getAt("message")

WS.verifyElementPropertyValue(response, "message", expect_rs)

//GlobalVariable.requestId = jsonResponse.getAt("data").getAt("id").toString()
//
//println "GlobalVariable.requestId: " + GlobalVariable.requestId

////lấy request ID bên merchant
// vào notification- lấy ID noti, lấy request ID Subscriber
//verify 2 request ID
//vào request Detail notification lấy phone merchant
//gán biến phone merchant/ request ID/ ShopID để paymerchant

