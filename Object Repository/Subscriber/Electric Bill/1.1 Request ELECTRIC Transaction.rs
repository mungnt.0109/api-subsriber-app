<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>1.1 Request ELECTRIC Transaction</name>
   <tag></tag>
   <elementGuidId>6dee1ef8-581a-4f42-a331-cedaf0f393ba</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;application/x-www-form-urlencoded&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;SENDERCLIENT&quot;,
      &quot;value&quot;: &quot;customer&quot;
    },
    {
      &quot;name&quot;: &quot;DEVICEID&quot;,
      &quot;value&quot;: &quot;7d884018af47058d&quot;
    },
    {
      &quot;name&quot;: &quot;MessageType&quot;,
      &quot;value&quot;: &quot;FO&quot;
    },
    {
      &quot;name&quot;: &quot;SERVICEIDEM&quot;,
      &quot;value&quot;: &quot;MESC-EM&quot;
    },
    {
      &quot;name&quot;: &quot;SERVICEID&quot;,
      &quot;value&quot;: &quot;5eddac255771fb73a4489a9b&quot;
    },
    {
      &quot;name&quot;: &quot;SENDERPHONE&quot;,
      &quot;value&quot;: &quot;${SENDERPHONE}&quot;
    },
    {
      &quot;name&quot;: &quot;MESSAGE&quot;,
      &quot;value&quot;: &quot;abc&quot;
    },
    {
      &quot;name&quot;: &quot;RECEIVERPHONE&quot;,
      &quot;value&quot;: &quot;MESCEM&quot;
    },
    {
      &quot;name&quot;: &quot;RECEIVERCLIENT&quot;,
      &quot;value&quot;: &quot;biller&quot;
    },
    {
      &quot;name&quot;: &quot;CURRENCY&quot;,
      &quot;value&quot;: &quot;MMK&quot;
    },
    {
      &quot;name&quot;: &quot;BILLID&quot;,
      &quot;value&quot;: &quot;${BILLID}&quot;
    },
    {
      &quot;name&quot;: &quot;METERNO&quot;,
      &quot;value&quot;: &quot;${METERNO}&quot;
    },
    {
      &quot;name&quot;: &quot;AMOUNTBILL&quot;,
      &quot;value&quot;: &quot;${AMOUNTBILL}&quot;
    },
    {
      &quot;name&quot;: &quot;MONTH&quot;,
      &quot;value&quot;: &quot;2&quot;
    },
    {
      &quot;name&quot;: &quot;YEAR&quot;,
      &quot;value&quot;: &quot;2021&quot;
    },
    {
      &quot;name&quot;: &quot;CONSUMERNAME&quot;,
      &quot;value&quot;: &quot;ဒေါ်မြတင်&quot;
    },
    {
      &quot;name&quot;: &quot;ADDRESS&quot;,
      &quot;value&quot;: &quot;ခ၀ါခ&quot;
    },
    {
      &quot;name&quot;: &quot;LEDGERNO&quot;,
      &quot;value&quot;: &quot;08/36/10&quot;
    },
    {
      &quot;name&quot;: &quot;DUODATE&quot;,
      &quot;value&quot;: &quot;2/28/2021 12:00:00 AM&quot;
    },
    {
      &quot;name&quot;: &quot;READINGDATE&quot;,
      &quot;value&quot;: &quot;1/5/2021 9:05:08 AM&quot;
    },
    {
      &quot;name&quot;: &quot;SERVICECHARGES&quot;,
      &quot;value&quot;: &quot;500.0000&quot;
    },
    {
      &quot;name&quot;: &quot;AMOUNT&quot;,
      &quot;value&quot;: &quot;1220&quot;
    },
    {
      &quot;name&quot;: &quot;TOTALUNITS&quot;,
      &quot;value&quot;: &quot;156.0000&quot;
    },
    {
      &quot;name&quot;: &quot;TRANSDESC&quot;,
      &quot;value&quot;: &quot;${TRANSDESC}&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>x-www-form-urlencoded</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/x-www-form-urlencoded</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer ${GlobalVariable.token}</value>
   </httpHeaderProperties>
   <katalonVersion>7.8.0</katalonVersion>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${GlobalVariable.link}api/transaction/requestTransaction</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>92291da2-b9f7-4351-be4a-2c447ffebd76</id>
      <masked>false</masked>
      <name>SENDERPHONE</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>edd004cd-14e9-446c-bdf7-97ae45854251</id>
      <masked>false</masked>
      <name>BILLID</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>7fdbab5c-ccc3-4d9e-9bcf-b331464d1411</id>
      <masked>false</masked>
      <name>METERNO</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>38ed735d-5928-4735-8319-e35ec2ceefba</id>
      <masked>false</masked>
      <name>AMOUNTBILL</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>8b7849ba-fa28-4922-9c0d-4c8f28e2d432</id>
      <masked>false</masked>
      <name>TRANSDESC</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
