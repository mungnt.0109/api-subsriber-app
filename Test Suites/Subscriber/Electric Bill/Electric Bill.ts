<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Electric Bill</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>6fde60e2-82ef-4906-834b-d191302385ce</testSuiteGuid>
   <testCaseLink>
      <guid>22051e0a-a633-49a1-a29a-34ad3fd6fa6d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Subscriber/Login/LoginSuccess</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>dd8406ed-0bb0-4799-90eb-2fe65bf016b9</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a6823430-de51-48de-94dc-830bc381c606</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>028d4483-eb33-4d46-80a6-98e998dd9986</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Subscriber/Electric Bill/Bill Payment - Success</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>c54060dc-ba25-4067-940b-9dafd958ac2a</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>22217e0c-82e9-404a-a182-585ad0a9e5d1</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>56031cfb-7925-40b0-8cf1-b47d0cc65810</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>02411ce0-b9b7-45c0-b83d-a103e001241b</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>af8ecbc6-10a5-41a3-9f9d-d352415d9bc0</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f903e694-84e5-44ba-97c3-1f089fa8e43f</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>20490dc2-cfe6-46ce-bfb5-105104982b5e</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>b2412e7c-c245-46ed-b578-0464ceca7d45</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Subscriber/Electric Bill/Bill Payment - Fai 1</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>6965711c-c421-45b7-8c7d-3502ac5a50af</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Subscriber/Electric Bill/Electric Bill - Fail 1</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>6965711c-c421-45b7-8c7d-3502ac5a50af</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>SENDERPHONE</value>
         <variableId>9e4326a7-694c-409b-bdce-32aff99ff20d</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>6965711c-c421-45b7-8c7d-3502ac5a50af</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>BILLID</value>
         <variableId>0c951dbe-f292-4668-b085-6f9c6496e509</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>6965711c-c421-45b7-8c7d-3502ac5a50af</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>METERNO</value>
         <variableId>7b18455a-cde5-44f5-b1bb-dcdcf2b24df6</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>6965711c-c421-45b7-8c7d-3502ac5a50af</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>AMOUNTBILL</value>
         <variableId>6f4080fc-7016-451d-8418-867f56f21fa0</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>6965711c-c421-45b7-8c7d-3502ac5a50af</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>TRANSDESC</value>
         <variableId>6a2dad21-8522-47a9-ad48-1679869c4b9f</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>6b1357f0-693d-49a3-b4ed-e7be60a37297</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>6965711c-c421-45b7-8c7d-3502ac5a50af</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>exp_message</value>
         <variableId>2a0af7c0-083e-41bd-abe7-6004a9139ea5</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
