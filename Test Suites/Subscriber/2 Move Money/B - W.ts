<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>B - W</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>8ea7419c-96b1-42ed-838e-ffb71568313a</testSuiteGuid>
   <testCaseLink>
      <guid>f6281edd-11a5-4c8d-8ee0-f4ea10ef66b5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Subscriber/Login/LoginSuccess</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>dd8406ed-0bb0-4799-90eb-2fe65bf016b9</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a6823430-de51-48de-94dc-830bc381c606</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>79b62387-fbe3-49b1-a619-2625e35608b2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Subscriber/Find Bank Account</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2279a932-4833-41fe-b98e-52fc2d8254f4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Subscriber/Move Money B- W/Bank - Wallet - Success</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DATA_COLUMN</type>
         <value></value>
         <variableId>959875b7-4023-4e2d-adec-f7f34f9d5d66</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DATA_COLUMN</type>
         <value></value>
         <variableId>535b8976-052a-47c0-834b-b433c541cf7e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DATA_COLUMN</type>
         <value></value>
         <variableId>6118417c-2c33-4cee-bef8-c76520ecb6bf</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DATA_COLUMN</type>
         <value></value>
         <variableId>7ed98a5c-73dd-413b-99a9-a3c12dee1eb2</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DATA_COLUMN</type>
         <value></value>
         <variableId>6a700d5a-1bfb-4ba2-a3f4-0514e836eaba</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>f7ab1f45-1966-4b08-8fab-e0b511b61f58</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Subscriber/Move Money B- W/Bank - Wallet - Fail 1</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>f9f4b3a4-5ba8-4f94-b8eb-f6af1562e16a</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Subscriber/Move money/Bank - Wallet - Fail 1</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>f9f4b3a4-5ba8-4f94-b8eb-f6af1562e16a</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>SENDERPHONE</value>
         <variableId>a1fb3a78-bd00-4b4a-81d0-dfc83629d5a7</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>f9f4b3a4-5ba8-4f94-b8eb-f6af1562e16a</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>AMOUNT</value>
         <variableId>22a3732a-0d34-4c58-b979-6eefaa69970d</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>f9f4b3a4-5ba8-4f94-b8eb-f6af1562e16a</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>OTP</value>
         <variableId>f07b2ca1-d654-4b96-b48a-852acf327191</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>f9f4b3a4-5ba8-4f94-b8eb-f6af1562e16a</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>PIN</value>
         <variableId>404440aa-fedd-4d15-afc8-7d57ab0fb2a5</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>f9f4b3a4-5ba8-4f94-b8eb-f6af1562e16a</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>exp_message</value>
         <variableId>aed88cc6-311a-43a0-848c-118ee6794cfc</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>9d5fff32-3b6a-4d74-92ff-d7b1a4595668</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Subscriber/Move Money B- W/Bank - Wallet - Fail 2</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>41adf4b1-dab9-4bc0-8686-02a11b0b82f0</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Subscriber/Move money/Bank - Wallet - Fail 2</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>41adf4b1-dab9-4bc0-8686-02a11b0b82f0</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>SENDERPHONE</value>
         <variableId>1a38f7de-b188-4b72-a824-3b3fc78550f6</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>41adf4b1-dab9-4bc0-8686-02a11b0b82f0</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>AMOUNT</value>
         <variableId>e3481b0b-65dd-4b8c-ae31-60bc62369ca2</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>41adf4b1-dab9-4bc0-8686-02a11b0b82f0</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>OTP</value>
         <variableId>ebe1a3ca-5e77-477b-adb7-3ceeacec9a5f</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>41adf4b1-dab9-4bc0-8686-02a11b0b82f0</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>PIN</value>
         <variableId>9554f8d5-756c-47bb-81cf-1536af8b935e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>41adf4b1-dab9-4bc0-8686-02a11b0b82f0</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>exp_message1</value>
         <variableId>e92dfbd6-687b-4e0c-b6b5-a34e689cabcf</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>41adf4b1-dab9-4bc0-8686-02a11b0b82f0</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>exp_message2</value>
         <variableId>b26f07a0-47ef-427e-85a9-36701b7d5b6a</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>41adf4b1-dab9-4bc0-8686-02a11b0b82f0</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>exp_message3</value>
         <variableId>386ead84-713a-410b-a2b8-c4875f57cb69</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
